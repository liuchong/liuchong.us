<?php
session_start();
if (isset($_POST["item"])) {
  $id = session_id(); $item = $_POST["item"]; $dir = votes_dir($item);
  if (!isset($_COOKIE["rc_vote_item"])) {
    file_put_contents("$dir/{$id}_{$_SERVER['REMOTE_ADDR']}", "keep");
    setcookie("rc_vote_item", $item, time() + 3600);
    header("Location: {$_SERVER['HTTP_REFERER']}"); die();
  }
}
function votes_dir($item) {
  $dir = "count_rc_logo_$item";
  if (!is_dir($dir)) mkdir($dir);
  return $dir;
}
function votes_count($item) {
  $count = 0;
  foreach (scandir(votes_dir($item)) as $filename)
    if ($filename != "." && $filename != "..") $count += 1;
  return $count;
}
?>
<!DOCTYPE html>
<html>
<meta charset="utf-8" />
<title>茸城之光 标志 投票</title>
<style><!--
body { margin: 0; padding: 0; background-image: url(assets/img/bg_r.png); }
div#content { background-image: url(assets/img/content_bg.png);
  background-color: White; margin-bottom: 369px; padding-top: 138px;
  background-repeat: no-repeat; background-position: center 63px; }
div#menubar { background-image: url(assets/img/menu_bg.png);
  background-color: White; background-repeat: no-repeat; }
div#banner { background-image: url(assets/img/banner_bg.png); }
div#header { background-image: url(assets/img/head_bg.png);
  background-position: center top; }
div#footer { text-align: center; color: White; font-size: 14px; }
div#page { margin: 0 auto; position: relative; }
h1 { margin: 0; position: absolute; top: 50px; left: 150px; width: 80%; }
h1 > a { display: block; width: 100%; height: 100%; text-indent: -999em; }
div.vote_item { position: absolute; text-align: center; width: 148px; }
div.vote_item > p { margin: 9px 0; color: Red; font-size: 14px; }
--></style>

<div id="page" style="min-width: 980px; max-width: 1280px;">
  <div id="header" style="height: 187px;">
    <h1 style="height: 137px;">
      <a title="茸城之光标志投票" href="/votes/rc_logo.php">茸城之光标志投票</a>
    </h1>
  </div>
  <div id="main" style="width: 983px; margin: 0 auto;">
    <div id="menubar" style="height: 52px;"></div>
    <div id="banner" style="height: 219px;"></div>
    <div id="content" style="height: 661px;">
      <div style="position: relative; height: 652px; ">
        <div class="vote_item" style="top: 155px; left: 61px;">
          <?php if (!isset($_COOKIE["rc_vote_item"])): ?>
          <form action="?" method="post">
            <input type="hidden" name="item" value="1" />
            <input type="submit" value="投我一票" /></form>
          <?php endif ?>
          <p><?php echo votes_count("1") ?>票
        </div>
        <div class="vote_item" style="top: 155px; left: 522px;">
          <?php if (!isset($_COOKIE["rc_vote_item"])): ?>
          <form action="?" method="post">
            <input type="hidden" name="item" value="2" />
            <input type="submit" value="投我一票" /></form>
          <?php endif ?>
          <p><?php echo votes_count("2") ?>票
        </div>
        <div class="vote_item" style="top: 473px; left: 61px;">
          <?php if (!isset($_COOKIE["rc_vote_item"])): ?>
          <form action="?" method="post">
            <input type="hidden" name="item" value="3" />
            <input type="submit" value="投我一票" /></form>
          <?php endif ?>
          <p><?php echo votes_count("3") ?>票
        </div>
        <div class="vote_item" style="top: 473px; left: 526px;">
          <?php if (!isset($_COOKIE["rc_vote_item"])): ?>
          <form action="?" method="post">
            <input type="hidden" name="item" value="4" />
            <input type="submit" value="投我一票" /></form>
          <?php endif ?>
          <p><?php echo votes_count("4") ?>票
        </div>
      </div>
    </div>
    <div id="footer" style="height: 161px;">
      <p>中共松江区委宣传部 版权所有</p>
      <p>地址: 松江区园中路一号（邮编: 201620）电话: 021-37735973
          传真: 021-37735463 电子邮件: sjwmb@126.com</p>
    </div>
  </div>
</div>
